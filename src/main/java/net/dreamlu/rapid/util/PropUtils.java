package net.dreamlu.rapid.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

/**
 * 配置文件帮助类
 * @author L.cm
 * email: 596392912@qq.com
 * site:http://www.dreamlu.net
 * @date 2014年9月10日 上午9:57:28
 */
public class PropUtils {

	/**
	 * 读取配置文件
	 * @param propName
	 * @return
	 */
	public static Properties loadProp (String propName) {
		Properties prop = new Properties();
		InputStream inStream = null;
		try {
			inStream = PropUtils.class.getClassLoader().getResourceAsStream(propName);
			prop.load(inStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(inStream);
		}
		return prop;
	}
}